package projectDay;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class AcmeTest {
	@Test
	public void test() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.get("https://acme-test.uipath.com/account/login");
		
		driver.findElementById("email").sendKeys("myprofessional12@gmail.com");
		driver.findElementById("password").sendKeys("5432167890");
		driver.findElementById("buttonLogin").click();
		
		Actions mouse_over_vendors = new Actions(driver);
		mouse_over_vendors.moveToElement(driver.findElementByXPath("(//button[@type='button'])[6]")).build().perform();
		Thread.sleep(2000);
		mouse_over_vendors.moveToElement(driver.findElementByXPath("//a[text()='Search for Vendor']")).click().build().perform();
		Thread.sleep(3000);
		
		driver.findElementById("vendorTaxID").sendKeys("RO254678");
		driver.findElementById("buttonSearch").click();
		
		String text = driver.findElementByXPath("//tbody/tr/td[text()='Softload Export']").getText();
		System.out.println(text);
		

	}

}
