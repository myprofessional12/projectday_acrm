package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class ViewLeadPage extends Annotations{ 

	public ViewLeadPage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.ID, using="viewLead_statusId_sp") WebElement eleVeriftFirstName;
	public LoginPage clickLogout(String data) {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		verifyExactText(eleVeriftFirstName, data);  
		return new LoginPage();

	}

}







